[![pipeline status](https://gitlab.com/grunlab/base-image/badges/main/pipeline.svg)](https://gitlab.com/grunlab/base-image/-/commits/main)

# GrunLab Base-Image

Base non-root container images.

Docs: https://docs.grunlab.net/images/base-image.md

## Debian 12

Base image: [debian:12][debian]

Format: docker

Supported architecture(s):
- arm64

GrunLab project(s) using this image:
- ...

## Ubuntu 22.04

Base image: [ubuntu:22.04][ubuntu]

Format: docker

Supported architecture(s):
- arm64

GrunLab project(s) using this image:
- ...

## Ubuntu 24.04

Base image: [ubuntu:24.04][ubuntu]

Format: docker

Supported architecture(s):
- arm64

GrunLab project(s) using this image:
- ...

[debian]: <https://hub.docker.com/_/debian>
[ubuntu]: <https://hub.docker.com/_/ubuntu>